# Skynet 入门实战  

项目讲解同步博客：[【Skynet 入门实战练习】](https://blog.csdn.net/qq_52678569/article/details/134528283)  


## 构建  

- `make build`  
- `make server`  

## 测试  
- `make client`  

## 支持指令

类型 | 名称 | 语法| 返回值 
--|--|--|--
`ws`|登录| `ws login username` | `{pid, msg, uid, username, lv, exp}` 
`ws`|获取信息| `ws get_userinfo` | `{uid, lv, username, exp}`
`ws`|获取用户名| `ws get_username` | `{username, pid}`
`ws`|设置用户名| `ws set_username new_name` | `{pid, msg}`
`gm`| 设置用户名 | `gm user setname new_name` | `{pid, msg, ok}`
`gm`|增加经验值| `gm user addexp experience` | `{pid, msg, ok}`
`gm`| 广播 | `gm user bmsg message` | `{pid, msg}`
`gm`| 模糊查询 | `gm user search username` | `{pid, msg, msg, ..., ok}`